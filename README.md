# Course notes

## Spring

It is a Java framework that helps us to delegate repetitive tasks and focus on business logic. There are four projects
that we will use for this development:

* *Spring Framework:* it is the core project. Transversal usage that allows to build enterprise applications.
* *Spring Boot:* Allows to create auto-contained and auto-configurable applications.
* *Spring Data:* Allows to integrate databases with our apps. It is an umbrella project that has many subprojects for each database (e.g. `Spring Data Cassandra` or `Spring Data MongoDB`).
* *Spring Security:* Authentication, Authorization and security management. It has many attack protection methods.

For more projects, check [Spring Projects](https://spring.io/projects).

## Spring Boot

* *Auto-contained applications:* an application is auto-contained when it has its own server and configurations. This allows instead of having a big unique applications to have many smaller ones.
* It can work with `Tomcat` (default), `Jetty` or `Undertow`.
* Includes a package manager like `Maven` or `Gradle`.

### Application properties

* [Common Application Properties](https://docs.spring.io/spring-boot/docs/current/reference/html/application-properties.html).
* It is possible to create custom properties.
* It can manage many profiles according to the environment (e.g. `dev` or `prod`).

## JPA

* It is a standard that all any ORM needs to follow to be able to connect with Java.
* The most common implementations are `Hibernate`, `TopLink`, `EclipseLink`, `ObjectDB`.
* Uses annotations for mapping Java objects to db objects (e.g. `@entity`, `@table`, `@column`, etc).
