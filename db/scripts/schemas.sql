-- -----------------------------------------------------
-- Table "CATEGORIES"
-- -----------------------------------------------------
CREATE TABLE CATEGORIES (
  "id_category" SERIAL NOT NULL,
  "description" VARCHAR(45) NOT NULL,
  "status" BOOLEAN NOT NULL,
  PRIMARY KEY ("id_category"));


-- -----------------------------------------------------
-- Table "PRODUCTS"
-- -----------------------------------------------------
CREATE TABLE PRODUCTS (
  "id_product" SERIAL NOT NULL,
  "product_name" VARCHAR(45) NULL,
  "id_category" INT NOT NULL,
  "bar_code" VARCHAR(150) NULL,
  "price" DECIMAL(16,2) NULL,
  "stock_quantity" INT NOT NULL,
  "status" BOOLEAN NULL,
  PRIMARY KEY ("id_product"),
  CONSTRAINT "fk_PRODUCTS_CATEGORIES"
    FOREIGN KEY ("id_category")
    REFERENCES CATEGORIES ("id_category")
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table "CLIENTS"
-- -----------------------------------------------------
CREATE TABLE CLIENTS (
  "id" VARCHAR(20) NOT NULL,
  "first_name" VARCHAR(40) NULL,
  "last_name" VARCHAR(100) NULL,
  "mobile_number" NUMERIC NULL,
  "address" VARCHAR(80) NULL,
  "email" VARCHAR(70) NULL,
  PRIMARY KEY ("id"));


-- -----------------------------------------------------
-- Table "PURCHASES"
-- -----------------------------------------------------
CREATE TABLE PURCHASES (
  "id_purchase" SERIAL NOT NULL,
  "id_client" VARCHAR(20) NOT NULL,
  "date" TIMESTAMP NULL,
  "payment_method" CHAR(1) NULL,
  "comment" VARCHAR(300) NULL,
  "status" CHAR(1) NULL,
  PRIMARY KEY ("id_purchase"),
  CONSTRAINT "fk_PURCHASES_CLIENTS1"
    FOREIGN KEY ("id_client")
    REFERENCES CLIENTS ("id")
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table "PURCHASES_PRODUCTS"
-- -----------------------------------------------------
CREATE TABLE PURCHASES_PRODUCTS (
  "id_purchase" INT NOT NULL,
  "id_product" INT NOT NULL,
  "quantity" INT NULL,
  "total" DECIMAL(16,2) NULL,
  "status" BOOLEAN NULL,
  PRIMARY KEY ("id_purchase", "id_product"),
  CONSTRAINT "fk_PURCHASES_PRODUCTS_PRODUCTS1"
    FOREIGN KEY ("id_product")
    REFERENCES PRODUCTS ("id_product")
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT "fk_PURCHASES_PRODUCTS_PURCHASES1"
    FOREIGN KEY ("id_purchase")
    REFERENCES PURCHASES ("id_purchase")
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);